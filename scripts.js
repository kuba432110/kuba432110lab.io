navigation.addEventListener
("navigate",navigateEvent=>
    {
        if(!navigateEvent.canIntercept||navigateEvent.hashChange||navigateEvent.downloadRequest||navigateEvent.formData)return;
        const url=new URL(navigateEvent.destination.url);
        navigateEvent.intercept
        (
            {handler:async()=>
                {
                    let path;
                    if(url.pathname=="/")
                        path="/root.html";
                    else
                        path="/ideas"+url.pathname+".html";
                    const html = await fetch(path).then((data) => data.text());
                    document.getElementById("root").innerHTML = html;
                }
            }
        );
    }
);
document.getElementById("dark").addEventListener
("click",()=>
    {
        if(!document.body.classList.contains("transition"))
            document.body.classList.add("transition");
        document.body.classList.toggle("dark");
        if(document.body.classList.contains("dark"))
            localStorage.setItem("theme","dark");
        else
            localStorage.setItem("theme","light");
    }
);
window.addEventListener
("scroll",()=>
    {
        document.getElementById("sidebar").style.opacity="1";
        setTimeout(()=>document.getElementById("sidebar").style.opacity="",1000);
    }
);
navigation.navigate(window.location.pathname);
if(localStorage.getItem("theme")=="dark")
    document.body.classList.add("dark");
